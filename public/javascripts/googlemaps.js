var directionsService;
var directionsDisplay;
var latLng = new google.maps.LatLng(lat,lng);

function initialize() {

    var mapcanvas = document.getElementById('map-canvas');
    var height = (window.innerHeight - document.getElementById('header').clientHeight) - 50;
    mapcanvas.style.height = height + 'px';

    var mapOptions = {
	center: latLng,
	zoom: zoom,
	panControl: true,
	zoomControl: true,
	zoomControlOptions: {
	    style: google.maps.ZoomControlStyle.LARGE
	},
	mapTypeControl: true,
	mapTypeControlOptions: {
	    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
	    labels: true
	},
	mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var map = new google.maps.Map(mapcanvas, mapOptions);

    var marker = new google.maps.Marker({
	position: latLng,
	map: map,
	title: venue
    });

    var info = new google.maps.InfoWindow({
	content: venue
    });
    info.open(map, marker);

    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsDisplay.setMap(map);

}

function calcRoute() {
    var request = {
	origin: document.getElementById('search').value,
	destination: latLng,
	travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) {
	if(status == google.maps.DirectionsStatus.OK) {
	    directionsDisplay.setDirections(result);
	}
	else
	    console.log(status);
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
