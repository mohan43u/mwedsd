// process all requests and route it accordingly

function handleroutes() {
    this.index = function(req, res) {
	res.render('index', {'page': 'index',
			     'title': 'Mohan weds Deepa',
			      'groom': 'Mohan',
			      'bride': 'Deepa',
			      'date': 'December 06, 2013',
			      'time': '6:00 AM to 7:30 AM',
			      'venue': 'Vijaya Mahal, South Mada Street (Near (A)Ranganathar Temple), Pallikonda, Vellore Dt., Tamilnadu'
			    });
    };
    this.location = function(req, res) {
	var lat = 12.91206;
	var lng = 78.93677;
	res.render('location', { 'page': 'location',
				 'title': 'Location',
				 'venue': 'Vijaya Mahal',
				 'lat': lat,
				 'lng': lng,
				 'zoom': 13});
    };
    this.wishes = function(req, res) {
	res.render('wishes', { 'page': 'wishes',
			       'title': 'Wishes from Friends',
			       'fbusername': 'mohan43u', // should not be the email-id we normally use to login into facebook
			       'fbeventname': 'Wedding' // this is the name of your wedding event in facebook
			       });
    };
    this.app = function(req, res) {
	res.render('app', { 'page': 'app',
			       'title': 'Application Details'
			       });
    };
}

exports = module.exports = function() {
    return new handleroutes();
}