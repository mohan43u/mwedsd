/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var handleroutesclass = require('./routes/handleroutes');
var http = require('http');
var path = require('path');
var stylus = require('stylus');
var jeet = require('jeet');


// initializing object
var app = express();
var handleroutes = handleroutesclass();

// all environments
app.set('env', 'development');
app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 3000);
app.set('host', process.env.OPENSHIFT_NODEJS_IP || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);

// jeet integration
function compile(str, path) {
    return stylus(str).set('filename', path).use(jeet());
}

app.use(stylus.middleware({src: path.join(__dirname, 'public'), compile: compile}));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

// handling routes
app.get('/', handleroutes.index);
app.get('/location', handleroutes.location);
app.get('/wishes', handleroutes.wishes);
app.get('/app', handleroutes.app);

// server
console.log("host:port: " + app.get('host') + ":" + app.get('port'));
http.createServer(app).listen(app.get('port'), app.get('host'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
